<?php

namespace Netmon\JsonApi\Traits;

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\GoneHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\LengthRequiredHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\PreconditionFailedHttpException;
use Symfony\Component\HttpKernel\Exception\PreconditionRequiredHttpException;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;
use Symfony\Component\HttpKernel\Exception\TooManyRequestsHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\Exception\UnsupportedMediaTypeHttpException;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Eloquent\RelationNotFoundException;
use Illuminate\Http\Request;
use Gate;

use Tobscure\JsonApi\Document;
use Tobscure\JsonApi\Collection;

trait CollectionResourceTrait {

    //json:api request parameter "filter"
    protected $filters = [];
    //json:api request parameter "page"
    protected $pagination = [];
    //total number of obejcts
    protected $total = 0;

    protected function initCollectionResorceTrait() {
        $this->filters = $this->parseFilters(\Request::get('filter'));
        $this->pagination = $this->parsePagination(\Request::get('page'));
    }

    /**
     * Index
     *
     * Display a listing of the resource.
     *
     * @Get("/")
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $model = $this->modelInstance();

        //check authorisation
        if(Gate::denies('index', $model)) {
            throw new AccessDeniedHttpException(trans('api.unauthorized'));
        }

        $model = $model->with($this->includes);
        $model = $this->applyFilters($model);
        $collection = $model->get();
        $total = $collection->count();
        $collection = $this->applyPagination($collection);

        //return new collection
        return $this->collectionResponse($collection, $total);
    }

    protected function collectionResponse($collection, $total, $statuscode = 200) {
        $collection = new Collection($collection, $this->serializerInstance());
        $collection->with($this->includes);

        // Create a new json:api document with the collection as data
        $document = new Document($collection);

        //add top level links
        if(Gate::allows('index', $this->model))
            $document->addLink('index', $this->baseURI);
        if(Gate::allows('store', $this->model))
            $document->addLink('create', $this->baseURI);

        if(!empty($this->pagination)) {
            $document = $this->addPaginationLinks(
                $document,
                'url',
                $this->pagination['number'],
                $this->pagination['size'],
                $total
            );
        }

        // Output the document as JSON.
        return response()->json($document)->setStatusCode($statuscode);
    }

    protected function parseFilters($rawFilters) {
        if(empty($rawFilters))
            return [];

        $filters = [];
        foreach($rawFilters as $key=>$filter) {
            $filters[$key] = explode(",", $filter);
        }
        return $filters;
    }

    protected function parsePagination($rawPagination) {
        if(empty($rawPagination))
            return;

        $pagination['number'] = isset($rawPagination['number'])
                                    ? $rawPagination['number']
                                    : 1;

        $pagination['size'] = isset($rawPagination['size'])
                                    ? $rawPagination['size']
                                    : 10;
        return $pagination;
    }

    protected function applyFilters($model) {
        $i = 0;
        foreach ($this->filters as $key=>$values) {
            foreach($values as $value) {
                if (strpos($value, '%') !== false) {
                    // unprecise search
                    $model = $model->where($key, 'LIKE', $value);
                } else {
                    // precise search
                    $model = $model->where($key, '=', $value);
                }
            }
            $i++;
        }
        return $model;
    }

    protected function applyPagination($collection) {
        if(empty($this->pagination))
            return $collection;

        $page = $collection->forPage(
                            $this->pagination['number'],
                            $this->pagination['size']
        );
        return $page->all();
    }

    private function addPaginationLinks($document, $resourceUrl, $pageNumber, $pageSize, $totalObjects) {
        $totalPagesNumber = ceil($totalObjects/$pageSize);

        if ($pageNumber > 1) {
            $this->addPaginationLink($document, 'prev', $resourceUrl, $pageNumber, $pageSize, $totalObjects);
        }

        if ($pageNumber<$totalPagesNumber) {
            $this->addPaginationLink($document, 'next', $resourceUrl, $pageNumber, $pageSize, $totalObjects);
        }

        return $document;
    }

    private function addPaginationLink($document, $name, $resourceUrl, $pageNumber, $pageSize, $totalObjects) {
        $document->addLink($name, $resourceUrl);

        return $document;
    }
}

?>
