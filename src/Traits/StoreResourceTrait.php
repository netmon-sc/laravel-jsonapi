<?php

namespace Netmon\JsonApi\Traits;

use Netmon\JsonApi\Exceptions\Exceptions\BodyValidationException;

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\GoneHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\LengthRequiredHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\PreconditionFailedHttpException;
use Symfony\Component\HttpKernel\Exception\PreconditionRequiredHttpException;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;
use Symfony\Component\HttpKernel\Exception\TooManyRequestsHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\Exception\UnsupportedMediaTypeHttpException;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Eloquent\RelationNotFoundException;
use Illuminate\Http\Request;
use Gate;

use Illuminate\Support\MessageBag;

use JsonSchema\SchemaStorage;
use JsonSchema\Validator;
use JsonSchema\Constraints\Factory;

trait StoreResourceTrait {
    use DocumentResourceTrait;

    protected function initStoreResorceTrait() {
        $this->initDocumentResourceTrait();
    }

    public function validateStoreRequestBody(Request $request) {
        $jsonBaseSchema = file_get_contents(base_path('vendor/netmon-server/laravel-jsonapi/resources/schemas/schema'));
        $jsonBaseSchemaObject = json_decode($jsonBaseSchema);

        $jsonCreateSchema = file_get_contents(base_path('vendor/netmon-server/laravel-jsonapi/resources/schemas/schema-create'));
        $jsonCreateSchemaObject = json_decode($jsonCreateSchema);

        // The SchemaStorage can resolve references, loading additional schemas from file as needed, etc.
        $schemaStorage = new SchemaStorage();

        // This does two things:
        // 1) Mutates $jsonSchemaObject to normalize the references (to file://mySchema#/definitions/integerData, etc)
        // 2) Tells $schemaStorage that references to file://mySchema... should be resolved by looking in $jsonSchemaObject
        $schemaStorage->addSchema('file://schema', $jsonBaseSchemaObject);
        $schemaStorage->addSchema('file://schema-create', $jsonCreateSchemaObject);

        // Provide $schemaStorage to the Validator so that references can be resolved during validation
        $jsonValidator = new Validator( new Factory($schemaStorage));

        $this->validateRequestBody($request, $jsonValidator, $jsonCreateSchemaObject);
    }

    public function validateUpdateRequestBody(Request $request) {
        $jsonBaseSchema = file_get_contents(base_path('vendor/netmon-server/laravel-jsonapi/resources/schemas/schema'));
        $jsonBaseSchemaObject = json_decode($jsonBaseSchema);

        $jsonUpdateSchema = file_get_contents(base_path('vendor/netmon-server/laravel-jsonapi/resources/schemas/schema-update'));
        $jsonUpdateSchemaObject = json_decode($jsonUpdateSchema);

        // The SchemaStorage can resolve references, loading additional schemas from file as needed, etc.
        $schemaStorage = new SchemaStorage();

        // This does two things:
        // 1) Mutates $jsonSchemaObject to normalize the references (to file://mySchema#/definitions/integerData, etc)
        // 2) Tells $schemaStorage that references to file://mySchema... should be resolved by looking in $jsonSchemaObject
        $schemaStorage->addSchema('file://schema', $jsonBaseSchemaObject);
        $schemaStorage->addSchema('file://schema-update', $jsonUpdateSchemaObject);

        // Provide $schemaStorage to the Validator so that references can be resolved during validation
        $jsonValidator = new Validator( new Factory($schemaStorage));

        $this->validateRequestBody($request, $jsonValidator, $jsonUpdateSchemaObject);
    }

    private function validateRequestBody(Request $request, Validator $jsonValidator, \StdClass $schemaObject) {
        $requestBody = $request->getContent();
        $requestBodyObject = json_decode($requestBody);

        // Do validation (use isValid() and getErrors() to check the result)
        $jsonValidator->check($requestBodyObject, $schemaObject);
        if(!$jsonValidator->isValid()) {
            $messageBag = new MessageBag();
            foreach($jsonValidator->getErrors() as $key=>$error) {
                $messageBag->add($key, $error['message']);
            }
            throw new BodyValidationException(null, $messageBag);
        }
    }

    /**
     * Show
     *
     * Display the specified resource.
     *
     * @Get("/{id}")
     * @param  int  $primaryKey
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = $this->modelInstance($id);

        try {
            $model = $model->with($this->includes)->firstOrFail();
        } catch (ModelNotFoundException $e) {
            throw new NotFoundHttpException(trans('api.document_not_found'));
        }/* catch (RelationNotFoundException $e) {
            \Log::info($this->includes);
        }*/

        //check authorisation
        if(Gate::denies('show', $model)) {
            throw new AccessDeniedHttpException(trans('api.unauthorized'));
        }

        //return new collection
        return $this->documentResponse($model);
    }

    /**
     * Store
     *
     * Store a newly created resource in storage.
     *
     * @Post("/")
     * @Request("name=string", contentType="application/x-www-form-urlencoded")
     * @Response(201, body={"id": 10, "name": "string"})
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //check if request has valid format
    	if(!$request->isJson()) {
    		throw new UnsupportedMediaTypeHttpException(trans('api.unsupported_media'));
    	}

        $this->validateStoreRequestBody($request);

    	$payloadAttributes = $request->input('data.attributes');

        $model = $this->modelInstance();
        $model->fill($payloadAttributes);
        if($request->has('data.id')) {
            $model->{$model->getKeyName()} = $request->input('data.id');
        }

        //check authorisation
        if(Gate::denies('store', $model)) {
            throw new AccessDeniedHttpException(trans('api.unauthorized'));
        }

    	//Create the resource. No authorisation needed.
        $modelName = (new \ReflectionClass($model))->getShortName();
        \Event::fire("api.".strtolower($modelName).".creating", [$model]);
		$model->save();
        \Event::fire("api.".strtolower($modelName).".created", [$model]);

        //return response
        return $this->documentResponse($model, 201);
    }

    /**
     * Update
     *
     * Update the specified resource in storage.
     *
     * @Put("/{id}")
     * @Request({"name": "string", "password": "string"})
     * @Response(200, body={"id": 10, "name": "string"})
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $primaryKey
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //check if request has valid format
    	if(!$request->isJson()) {
    		throw new UnsupportedMediaTypeHttpException(trans('api.unsupported_media'));
    	}

        $this->validateUpdateRequestBody($request);

        $model = $this->modelInstance($id);

        try {
            $model = $model->with($this->includes)->firstOrFail();
        } catch (ModelNotFoundException $e) {
            throw new NotFoundHttpException(trans('api.document_not_found'));
        }

    	$payloadAttributes = $request->input('data.attributes');
        $model->fill($payloadAttributes);
        if($request->has('data.id')) {
            $model->{$model->getKeyName()} = $request->input('data.id');
        }

        //check authorisation
        if(Gate::denies('update', $model)) {
            throw new AccessDeniedHttpException(trans('api.unauthorized'));
        }

    	//Update the resource.
		$model->save();

        //return response
        return $this->documentResponse($model, 200);
    }

    /**
     * Destroy
     *
     * Remove the specified resource from storage.
     *
     * @Delete("/{id}")
     * @Response(204)
     *
     * @param  int  $primaryKey
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = $this->modelInstance($id);

        try {
            $model = $model->with($this->includes)->firstOrFail();
        } catch (ModelNotFoundException $e) {
            throw new NotFoundHttpException(trans('api.document_not_found'));
        }

        //check authorisation
        if(Gate::denies('destroy', $model)) {
            throw new AccessDeniedHttpException(trans('api.unauthorized'));
        }

    	//delete the resource
		$model->delete();

    	//on success: return empty response with code 204
        return response()->noContent();
    }
}

?>
