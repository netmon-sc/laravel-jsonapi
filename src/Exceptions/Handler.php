<?php

namespace Netmon\JsonApi\Exceptions;

use Exception;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Contracts\Container\Container;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\FatalErrorException;

use Tobscure\JsonApi\ErrorHandler;
use Tobscure\JsonApi\Document;
use Tobscure\JsonApi\Exception\Handler\InvalidParameterExceptionHandler;
use Tobscure\JsonApi\Exception\Handler\FallbackExceptionHandler;

use Netmon\JsonApi\Exceptions\Handler\ForbiddenExceptionHandler;
use Netmon\JsonApi\Exceptions\Handler\NotFoundExceptionHandler;
use Netmon\JsonApi\Exceptions\Handler\MethodNotAllowedExceptionHandler;
use Netmon\JsonApi\Exceptions\Handler\BadRequestExceptionHandler;

class Handler extends ExceptionHandler
{
    protected $errorHandler;

    public function __construct(Container $container) {
        $this->errorHandler = new ErrorHandler();
        parent::__construct($container);
    }

    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        return parent::report($e);
    }

    protected function registerHandlers() {
        $this->errorHandler->registerHandler(new InvalidParameterExceptionHandler);
        $this->errorHandler->registerHandler(new ForbiddenExceptionHandler);
        $this->errorHandler->registerHandler(new NotFoundExceptionHandler);
        $this->errorHandler->registerHandler(new MethodNotAllowedExceptionHandler);
        $this->errorHandler->registerHandler(new BadRequestExceptionHandler);
        $this->errorHandler->registerHandler(new FallbackExceptionHandler(false));
    }

    public function render($request, Exception $e) {
        $response = $this->errorHandler->handle($e);

        $document = new Document;
        $document->setErrors($response->getErrors());
        return response($document, $response->getStatus());
    }
}
