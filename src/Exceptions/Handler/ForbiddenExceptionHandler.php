<?php

namespace Netmon\JsonApi\Exceptions\Handler;

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

use Tobscure\JsonApi\Exception\Handler\ExceptionHandlerInterface;
use Tobscure\JsonApi\Exception\Handler\ResponseBag;

use Exception;

class ForbiddenExceptionHandler implements ExceptionHandlerInterface
{
    /**
     * {@inheritdoc}
     */
    public function manages(Exception $e)
    {
        return $e instanceof AccessDeniedHttpException;
    }
    /**
     * {@inheritdoc}
     */
    public function handle(Exception $e)
    {
        $status = 403;
        return new ResponseBag($status, [
            [
                'status' => $status,
                'code' => 'forbidden',
                'detail' => $e->getMessage()
            ]
        ]);
    }
}
