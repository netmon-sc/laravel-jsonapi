<?php

namespace Netmon\JsonApi\Exceptions\Handler;

use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

use Tobscure\JsonApi\Exception\Handler\ExceptionHandlerInterface;
use Tobscure\JsonApi\Exception\Handler\ResponseBag;

use Exception;

class MethodNotAllowedExceptionHandler implements ExceptionHandlerInterface
{
    /**
     * {@inheritdoc}
     */
    public function manages(Exception $e)
    {
        return $e instanceof MethodNotAllowedHttpException;
    }
    /**
     * {@inheritdoc}
     */
    public function handle(Exception $e)
    {
        $status = 405;
        return new ResponseBag($status, [
            [
                'status' => $status,
                'code' => 'method_not_allowed',
                'detail' => $e->getMessage()
            ]
        ]);
    }
}
