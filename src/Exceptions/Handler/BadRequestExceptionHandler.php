<?php

namespace Netmon\JsonApi\Exceptions\Handler;

use Illuminate\Database\Eloquent\RelationNotFoundException;
use Netmon\JsonApi\Exceptions\Exceptions\BodyValidationException;

use Tobscure\JsonApi\Exception\Handler\ExceptionHandlerInterface;
use Tobscure\JsonApi\Exception\Handler\ResponseBag;

use Exception;

class BadRequestExceptionHandler implements ExceptionHandlerInterface
{
    /**
     * {@inheritdoc}
     */
    public function manages(Exception $e)
    {
        return (    $e instanceof RelationNotFoundException
                    || $e instanceof BodyValidationException);
    }
    /**
     * {@inheritdoc}
     */
    public function handle(Exception $e)
    {
        $status = 400;
        $errors = [];


        if($e instanceof RelationNotFoundException) {
            $errors[] = [
                    'status' => $status,
                    'detail' => $e->getMessage()
            ];
        }

        if($e instanceof BodyValidationException) {
            $messageBag = $e->getValidationErrors();
            foreach($messageBag->getMessages() as $key=>$errorMessage) {
                $errors[] = [
                    'status' => $status,
                    'code' => "$key",
                    'detail' => $errorMessage
                ];
            }
        }

        return new ResponseBag($status, $errors);
    }
}
