<?php

namespace Netmon\JsonApi\Exceptions\Handler;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use Tobscure\JsonApi\Exception\Handler\ExceptionHandlerInterface;
use Tobscure\JsonApi\Exception\Handler\ResponseBag;

use Exception;

class NotFoundExceptionHandler implements ExceptionHandlerInterface
{
    /**
     * {@inheritdoc}
     */
    public function manages(Exception $e)
    {
        return ($e instanceof NotFoundHttpException
                || $e instanceof ModelNotFoundException);
    }
    /**
     * {@inheritdoc}
     */
    public function handle(Exception $e)
    {
        $status = 404;
        return new ResponseBag($status, [
            [
                'status' => $status,
                'code' => 'not_found',
                'detail' => $e->getMessage()
            ]
        ]);
    }
}
