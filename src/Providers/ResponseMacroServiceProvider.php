<?php

namespace Netmon\JsonApi\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Routing\ResponseFactory;

class ResponseMacroServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @param  ResponseFactory  $factory
     * @return void
     */
    public function boot(ResponseFactory $factory)
    {
        $factory->macro('noContent', function () use ($factory) {
            return $factory->make("", 204);
        });

        $factory->macro('created', function () use ($factory) {
            return $factory->make("", 201);
        });

        $factory->macro('item', function ($item) use ($factory) {
            return $factory->json($item->toArray());
        });

        $factory->macro('collection', function ($collection) use ($factory) {
            return $factory->json($collection->toArray());
        });
    }

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
