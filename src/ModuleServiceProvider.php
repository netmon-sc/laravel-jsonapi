<?php namespace Netmon\JsonApi;

use Illuminate\Support\ServiceProvider;

use Netmon\JsonApi\Serializers\BasicSerializer;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        BasicSerializer::setContainer($this->app);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //register all the service provider this user needs
        $this->app->register(\Netmon\JsonApi\Providers\ResponseMacroServiceProvider::class);
    }
}
