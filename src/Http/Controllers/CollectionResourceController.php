<?php

namespace Netmon\JsonApi\Http\Controllers;

use Netmon\JsonApi\Traits\CollectionResourceTrait;

abstract class CollectionResourceController extends JsonApiController
{
    use CollectionResourceTrait;

    public function __construct() {
        $this->initCollectionResourceTrait();

        parent::__construct();
    }
}

?>
