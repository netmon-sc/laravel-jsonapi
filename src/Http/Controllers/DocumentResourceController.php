<?php

namespace Netmon\JsonApi\Http\Controllers;

use Netmon\JsonApi\Traits\DocumentResourceTrait;

abstract class DocumentResourceController extends JsonApiController
{
    use DocumentResourceTrait;

    public function __construct() {
        $this->initDocumentResourceTrait();

        parent::__construct();
    }
}

?>
