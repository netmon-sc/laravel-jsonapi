<?php

namespace Netmon\JsonApi\Http\Controllers;

use Illuminate\Routing\Controller;

use Gate;

use Tobscure\JsonApi\Document;
use Tobscure\JsonApi\Collection;
use Tobscure\JsonApi\Resource;

abstract class JsonApiController extends Controller
{
    //Controller to model mapping
    protected $model = null;
    //Controller to serializer mapping
    protected $serializer = null;
    //Name of the resource this controller is used for
    protected $resource = null;
    //Base URI of the resource
    protected $baseURI = null;

    //json:api request parameter "include"
    protected $includes = [];

    public function __construct() {
        $this->model = $this->model();
        $this->serializer = $this->serializer();
        $this->resource = $this->resource();
        $this->baseURI = config('app.url')."/{$this->resource}";

        $this->includes = $this->parseIncludes(\Request::get('include'));
    }

    public abstract function model();
    public abstract function serializer();
    public abstract function resource();

    /**
     * Returns a new instance of the model associated to this controller.
     */
    protected function modelInstance($id = null) {
        $model = new $this->model;

        if(!is_null($id)) {
            return $model::where($model->getKeyName(), $id);
        }
        return new $model;
    }

    /**
     * Returns a new instance of the serializer associated to this controller.
     */
    protected function serializerInstance() {
        $serializer = $this->serializer;
        return new $serializer;
    }

    protected function parseIncludes($rawIncludes) {
        return (!empty($rawIncludes)) ?  explode(",", $rawIncludes) : [];
    }
}
