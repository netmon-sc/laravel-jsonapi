<?php

namespace Netmon\JsonApi\Http\Controllers;

use Netmon\JsonApi\Traits\StoreResourceTrait;
use Netmon\JsonApi\Traits\CollectionResourceTrait;

abstract class DefaultResourceController extends JsonApiController
{
    use StoreResourceTrait;
    use CollectionResourceTrait;

    public function __construct() {
        $this->initStoreResorceTrait();
        $this->initCollectionResorceTrait();

        parent::__construct();
    }
}

?>
